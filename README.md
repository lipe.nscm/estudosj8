1 - Expressões Lambdas
1.1 O que são ?
Métodos anônimos, funções sem nome que permitem a implementação com altos níveis de abstração em que uma função, pode receber como parâmetro, outra função, assim como *funções de ordem superior(1)*.
Bastante presente no paradigma funcional.

2 - Interfaces funcionais
2.1 Utilizam o padrão SAM (Single Abstract Method);
2.2 Implementam interfaces Function, Consumer, Predicate;


Link : 

A: - *Funções de ordem superior sao funções em que:*
A:a. Temos outra função como argumento;
A:b. Ou, produz uma função como resultado.

http://www.decom.ufop.br/romildo/2014-1/bcc222/practices/p15-superior.pdf

B: - *Paradigma Funcional* é um paradigma de programação em que trata a operação como um conjunto de funções evitando estados ou dados mutáveis.
Digamos que temos uma valor dado armazenado em nosso sistema X, e que temos uma função que o divide por 3, logo teremos :
f(x) = x/3;
o valor X ainda estará na memória, e caso a função seja chamada, ela também existirá em memória ao mesmo tempo.

https://elixirschool.com/pt/


2. Stream

a. Não é uma estrutura de dados
b. Uma interface que não armazena, apenas processa a coleção de dados fornecida;
c. Utiliza fortemente as expressões lambda;
d. Não há acesso indexado;
e. Facilmente convertido para matrizes e listas;
f. Suporta lazy access;
g. Pipeline - Operações de pipeline retornam, muitas vezes, uma stream de si mesmas. 
h. Iteração - Com várias instruções encadeadas, formamos uma canalização, com operações em iterações internas sem explicitar ao contrário das Collections.