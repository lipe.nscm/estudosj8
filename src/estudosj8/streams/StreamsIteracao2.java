package estudosj8.streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class StreamsIteracao2 {
		
	public static void main(String[] args) {
			
//		A iteração interna, é a terceira geração de iteradores
//		de coleções
		
		List<Integer> lista = Arrays.asList(1,2,3,4,5,6,7,8);
		
		List<String> lista2 = Arrays.asList("Pernambuco","Paraíba", "Rio Grande do Norte", "Alagoas", "Sergipe", "Ceará");
		
		System.out.println("Números pares");
		filtro(lista, x -> x%2==0 );
		System.out.println("Números ímpares");
		filtro(lista, x -> x%2 !=0);
		System.out.println("Estados que começam com a letra P");
		filtro2(lista2, x -> x.startsWith("P"));

		
		}
	//referencia direta no filter como referência de método
	public static void filtro(List<Integer> lista, Predicate<Integer> condicao) {
		lista.stream().filter(condicao::test).forEach(i -> System.out.println(i));
	}
	
	public static void filtro2(List<String> lista, Predicate<String> condicao) {
		lista.stream().filter((s) -> condicao.test(s)).forEach(s -> System.out.println(s));
	}
	
	
}
