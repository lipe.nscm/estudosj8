package estudosj8.streams;

import java.util.Arrays;
import java.util.List;

public class Streams {
	
		public static void main(String[] args) {
			Message c = x -> System.out.println(x);
			List<String> lista = Arrays.asList("Pernambuco","Paraíba", "Rio Grande do Norte", "Alagoas", "Sergipe", "Ceará");
			
			//Iteração Explícita
			c.call("Iteração explícita de uma lista");
			for(String s : lista)
				System.out.println(s);
			
			//Itera��o dentro de uma Stream
			c.call("\n\nIteração implícita através de uma interface stream, utilizando o forEach");
			lista.forEach(x -> System.out.println(x));
			
			c.call("\n\nIteração implícita através de uma interface stream, utilizando o forEach com outra forma de chamada");			
			lista.forEach(System.out::println);
	
		
			
			
		}
		
	
} 


interface Message{
	void call(String s);
}