package estudosj8.streams;

import java.util.Arrays;
import java.util.List;

public class StreamsIteracao {
		
	public static void main(String[] args) {
			
//		A iteração interna, é a terceira geração de iteradores
//		de coleções
		
		List<String> itens = Arrays.asList("Item 1", "Item 2","Item 3", "Item 4");
		
		//Iteração explícita
		for(String str : itens)	
			System.out.println(str);
		
		//utilizando forma mais recente, utilizando o ForEach
		//utilizando paralelismo
		itens.forEach(str -> System.out.println(str));
		
		}
}
