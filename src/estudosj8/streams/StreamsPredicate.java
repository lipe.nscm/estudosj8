package estudosj8.streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class StreamsPredicate {
//utilizando a interface predicate
	public static void main(String[] args) {
		List<Integer> lista = Arrays.asList(1,2,3,4,5,6,7,8);
		
		List<String> lista2 = Arrays.asList("Pernambuco","Paraíba", "Rio Grande do Norte", "Alagoas", "Sergipe", "Ceará");
		
		System.out.println("Números pares");
		filtro(lista, (s) -> (s%2)==0);
		
		System.out.println("Números ímpares");
		filtro(lista, (s) -> (s%2)!=0);
		
	
		System.out.println("Iterando por dentro de uma função inline ");
		lista2.stream().filter((o) -> o.startsWith("P")).forEach(o -> System.out.println(o));
	}
	
	public static void filtro(List<Integer> lista, Predicate<Integer> condicao) {
		for(Integer i: lista)
			if(condicao.test(i))
				System.out.println(i);
	}
	

	
	
}
