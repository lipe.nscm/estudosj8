package estudosj8.streams;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public class StreamsContinuacao {
	public static void main(String[] args) {
		

		//Supplier -> Interface funcional que possui como seu método funcional
		// uma função T get() , onde a mesma não recebe parâmetros e que entrega um valor.
				
		Stream.generate(() -> new Date())
		.limit(5)
			.map(x -> new SimpleDateFormat("dd/MMMM/YYYY hh:mm:ss").format(x))
			.forEach(x -> System.out.println(x));;
		//observe que em nossa função lambda, dentro do generate , não estamos passando nenhum parâmetro
		//apenas estamos solicitando que ele gere uma nova data, agindo assim como uma interface
		//supplier.
		
		//Consumer - > Interface funciona que possui como seu método funcional 
		//uma função void accepter(T t), onde a mesma recebe um T como parâmetro
		//não devendo T ser um nulo e não irá retornar nada. É um void né!
		//O método .forEach() do stream recebe um Consumer, por exemplo o sysout que
		//que usamos é um exemplo de Consumer, pois não retorna nada ao usuário
		//apenas escreve no console
			//BiConsumer -> A mesma coisa, só que ele aceita dois parâmetos ao invés de um;
			//uma das utilidades é a de comparar duas listas;
			
		//Function -> Interface funcional que recebe um T e retorna um R, função mais básica possível;
		//BiFunction - > Recebe dois valores e retorna apenas um valor.
			
			Stream.generate(() -> new Random().nextInt())
				.filter(x -> x%2==0)
				.limit(10)
				.map(x -> x.doubleValue())
				.forEach(System.out::println);;
		
		List<Integer> listaInteiros = Arrays.asList(1,2,3,4,5,7,8,9);
		List<Integer>outraListaInteiros = Arrays.asList(2,3,3,4,6,7,10);
		
		
		BiConsumer<List<Integer>, List<Integer>> equals = (l1, l2) -> {
			if(l1.size()==l2.size())
				System.out.println("são do mesmo tamanho");
			else
				System.out.println("são de tamanhos diferentes");
		};
		

		
		
		listaInteiros.stream();
		
	}
	

}


	

