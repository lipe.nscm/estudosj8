package estudosj8.optional;

import java.util.Optional;

public class ExemplosOptional {
public static void main(String[] args) {
		
		String entrada = "9";
		Optional<Integer> numero = convertStrToInteger(entrada);
		
		String entrada2 = "Jato";
		
		Optional<Integer> numero2 = convertStrToInteger(entrada2);
		
		//utilizando is present
		if(numero.isPresent())
			System.out.println(numero.get());
		else {
			System.out.println("Impossível realizar conversão");
		}
		
		
		//utilizando o método ifPresent
		//o método ifPresent recebe um Consumer
		numero.ifPresent(n -> System.out.println(n));
		//exemplificando com um valor que não pode 
		//ser convertido para inteiro
		//neste caso, o método consumidor não será
		//executado
		numero2.ifPresent(x -> System.out.println(x));
		
		//utilizando o método orElse
		//que, em caso de retorno vazio
		//irá retornar um valor default
		System.out.println(numero.orElse(4));
		
		//exemplificando quando passo uma palavra no 
		//lugar de um valor que pudesse ser convertido 
		//para um número inteiro
		System.out.println(numero2.orElse(4));
		
		
		//utilizando o orElseGet
		//que recebe uma função lambda como parâmetro
		//para retornar um valor ou uma ação
		System.out.println(numero.orElseGet(() -> 4));
		System.out.println(numero2.orElseGet(() ->{return retornaInteiro();}));
		
		
		//utilizando o orElseThrow
		//onde ao invés dele retornar um valor
		//ele retorna uma função lambda para 
		//lançamento de uma exceção
		System.out.println(numero.orElseThrow(() -> new NullPointerException("Lançando uma Exception: Valor vazio!")));
		System.out.println(numero2.orElseThrow(() -> new NullPointerException("Lançando uma Exception: Valor vazio!")));
		
	}
	
	public static Optional<Integer> convertStrToInteger(String in){
		try {
			Integer retorno =Integer.valueOf(in);
			return Optional.of(retorno);
		} catch (Exception e) {
			return Optional.empty();
		}
		
	}
	
	public static Integer retornaInteiro() {
		System.out.println("Função pesada");
		return 545;
	}
}
