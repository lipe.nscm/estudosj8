package estudosj8.lambda;

public class SintaxeLambda2 {

	public static void main(String[] args) {
	
		
 Num x , y, z, a;

 	x = () -> 555;
		System.out.println(x.getValue());
	y = () -> 88*3-54;	
		System.out.println(y.getValue());
	z = () -> 4 * 3;
		System.out.println(z.getValue());
	a = () -> 4 * 5 ;
		System.out.println(a.getValue());
		
		
	ValorNumerico isPar = n -> n % 2 == 0;
		
	System.out.println(isPar.teste(x.getValue()));
		System.out.println(isPar.teste(y.getValue()));
			System.out.println(isPar.teste(z.getValue()));
				System.out.println(isPar.teste(a.getValue()));
		
	}
	
	interface Num {
		
		int getValue();
	}
	
	interface ValorNumerico{
		boolean teste(int n);
	}
}
