package estudosj8.lambda;

public class SintaxeLambda {

	public static void main(String[] args) {
		//-parametros-     -operador- -operaçãoo-  
		//(int x ,   int y)   ->      {x+y};
		
		Num x , y, z, a;
		x = () -> 555.5;
		//onde
		//x = a declaração da variável com seu sinal de atribuição
		//()  um construtor vazio, pois não  há parâmetros
		// -> operador que indica onde ocorrerá a ação do método
		// 555.5  onde estará o bloco de comandos.
		
		System.out.println(x.getValue());
		
		//outro exemplo
		y = () -> 45.2*3.2-54.0;
		//observe que passei uma operação matemática que irá entregar um valor a ser colocado 
		//dentro da minha variável e o compilador irá se virar para interpretar e entregar 
		//o resultado dentro do meu método funcional getValue.
		System.out.println(y.getValue());
		
		//mais um exemplo
		z = () -> 4 * 3.4;
		//observe que estou multiplicando um inteiro por um double e no entanto
		//ele irá fazer a conversão implícita do tipo int para o tipo double a fim de satisfazer
		//a operação
		System.out.println(z.getValue());
		
		//maaaais um exemplo de conversão
		//em que ele executa uma conversão implícita dos dois parâmetros para double
		a = () -> 4 * 5 ;
		System.out.println(a.getValue());
		
	}
	
	interface Num {
		
		double getValue();
	}
}
