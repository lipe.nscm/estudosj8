package estudosj8.lambda;

public class Lambda {

	public static void main(String[] args) {
		 
		Runnable rnbl = new Runnable() {
			
			@Override
			public void run() {
				System.out.println("Implementando sem lambda o método abstrato run()");
			}
		};
		//fim
		
		Runnable rnbl2 = () -> System.out.println("Implementando usando lambda");
		
		rnbl.run();
		rnbl2.run();
		
	}
}
