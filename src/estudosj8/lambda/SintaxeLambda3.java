package estudosj8.lambda;

public class SintaxeLambda3 {

	public static void main(String[] args) {
	
		
 Num x , y, z, a;

 	x = () -> 555;
		System.out.println(x.getValue());
	y = () -> 88*3-54;	
		System.out.println(y.getValue());
	z = () -> 4 * 3;
		System.out.println(z.getValue());
	a = () -> 4 * 5 ;
		System.out.println(a.getValue());
		
		
	ValorNumerico isPar = n -> n % 2 == 0;

	System.out.println("==========Verificando se os números são pares============");
	System.out.println(isPar.teste(x.getValue()));
		System.out.println(isPar.teste(y.getValue()));
			System.out.println(isPar.teste(z.getValue()));
				System.out.println(isPar.teste(a.getValue()));
		
				//continuamos daqui :)
				
				ValorNumerico2 isDiv = (b, c) -> (b%c)==0;
				System.out.println("==========Verificando se os n�meros são divisíveis entre si============");
				//x com x
				System.out.println(isDiv.teste(x.getValue(), x.getValue()));
				//y com x
				System.out.println(isDiv.teste(y.getValue(), x.getValue()));
				//z com z	
				System.out.println(isDiv.teste(z.getValue(), z.getValue()));
				// a com z
				System.out.println(isDiv.teste(a.getValue(), z.getValue()));
				System.out.println("==========Alterando a forma da implementação============");
				
				ValorNumerico2 isDiv2 = (b,c) -> {
					int w = b + c;
					return w > 100;
				};
				System.out.println("==========Verificando se os valores são maiores do que 100============");
				//x com x
				System.out.println(isDiv2.teste(x.getValue(), x.getValue()));
				//y com x
				System.out.println(isDiv2.teste(y.getValue(), x.getValue()));
				//z com z	
				System.out.println(isDiv2.teste(z.getValue(), z.getValue()));
				// a com z
				System.out.println(isDiv2.teste(a.getValue(), z.getValue()));
				
				
				
	}
	//declaração das interfaces que estamos utilizando
	interface Num {
		
		int getValue();
	}
	
	interface ValorNumerico{
		boolean teste(int n);
	}
	
	interface ValorNumerico2{
		boolean teste(int x, int y);
	}
	
	
}
